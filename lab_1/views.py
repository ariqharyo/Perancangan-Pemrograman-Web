from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = ['Ariq Haryo Setiaki', "Ali Yusuf", "M. Faisal"] # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
npm = [1706043853, 1706044061, 1706039502] # TODO Implement this
kuliah = "Universitas Indonesia"
hobi = ["Bermain catur", "Tidur", "Nonton Film"]
deskripsi = ["Saya tampan", "Saya tidak tampan", "Saya PIC DS Acad"]
birth_date = date(1999,7,29)

# Create your views here.
def index(request):
    response = {'name': mhs_name[0], 'name1' : mhs_name[1], 'name2' : mhs_name[2],  'kuliah': kuliah, 'npm': npm[0], 'npm1' : npm[1], 'npm2' : npm[2],
                'hobi' : hobi[0], 'hobi1' : hobi[1], 'hobi2' : hobi[2], 'deskripsi' : deskripsi[0], 'deskripsi1' : deskripsi[1], 'deskripsi2' : deskripsi[2],
                'age': calculate_age(birth_date.year) }
                
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0

